# chiplet

chiplet is a small wrapper around ansible, and is geared towards being used to perform basic administration tasks on a PocketCHIP. It is suitable for any type of computer, though!

# Usage

`chiplet <playbook> <VARNAME>=<VARVALUE>`
